import math
import openerp.addons.product.product
import logging
import time
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from openerp.tools import  DEFAULT_SERVER_DATETIME_FORMAT

class good_practice(osv.osv):
    _name = 'good.practice'
    _description = 'Good Practice'
    _columns = {
              'contract_name':fields.char('Contract Name',required=True),
              'contract_no':fields.char('Contract Number'),
              'good_practice_no':fields.char('Good Practice No'),
              'date':fields.date('Date'),
              'description_finding':fields.text('Description of findings :'),
              'what_benefits':fields.text('What are the benefits'),
              'any_draw_backs':fields.text('Are there any Draw backs'),
              'where_the_idea':fields.text('Where the idea came from?'),

    }
    def action_print_good_practice(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids)[0]
        datas = {
             'ids': [],
             'model': 'ir.ui.menu',
             'form': data
                }
        # print datas,'?????????????????????????????????????????????????????????????????????'
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'Bam.report_good_practice_temp',
            'datas': datas,
            }

good_practice()