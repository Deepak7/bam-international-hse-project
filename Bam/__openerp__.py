{
    'name' : 'BAM',
    'version':'8.0',
    'sequence': 1,
    'author' : 'Azinova Technologies pvt ltd',
    'category' : 'Azinova',
    'depends':['base','base_action_rule','base_setup','mail','resource','board','fetchmail',],
    'update_xml':['bam_view.xml','good_practice.xml','safety_violation.xml','incident_general_details.xml',
                  'bam_dashboard.xml','safety_management_system.xml','bam_master.xml','risk_assessment.xml',
                  'legal_requirement.xml',
                  'report/good_practice_temp.xml','bam_menu.xml',

                 ],
    'installable':True,
    'application':True,
    'auto_install':False,
}
