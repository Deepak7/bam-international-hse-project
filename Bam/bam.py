import math
import openerp.addons.product.product
import logging
import time
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from openerp.tools import  DEFAULT_SERVER_DATETIME_FORMAT

class bam_view(osv.osv):
    _name = 'bam.view'
    _description = 'bam.view'
    _columns = {
              'project_name':fields.many2one('project.type','Project'),
              'code':fields.char('Reference',size=16),
    }


bam_view()
class project_details(osv.osv):
    _name='project.details'
    _description = 'Project Details'
    _columns = {
        'project_id':fields.char('Project Id'),
        'project_name':fields.many2one('project.type','Project Name'),
        'project_type':fields.many2one('type.of.projects','Project Type'),
        'project_geo_area':fields.char('Project Area'),
        'client':fields.many2one('client.master','Client'),
        'contract_period':fields.char('Contract Period'),
        'project_desc':fields.text('Contract Description'),
        'date':fields.date('Date'),
        'status': fields.selection([('new', 'New'),('ongoing','Ongoing'),('completed', 'Completed')],string='Status', readonly=True),
    }
project_details()

class employee_details(osv.osv):
    _name='employee.details'
    _description = 'Employee Details'

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image)
        return result

    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

    _columns = {
        'emp_no':fields.char('Employee No'),
        'employee_name':fields.char('Employee Name'),
        'designation':fields.char('Designation'),
        'joining_date':fields.date('Date of Joining'),
        'image': fields.binary("Image",
                            help="This field holds the image used as image for our customers, limited to 1024x1024px."),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
                            string="Image (auto-resized to 128x128):", type="binary", multi="_get_image",
                            store={
                                   'employee.details': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
                                   },help="Medium-sized image of the category. It is automatically "\
                            "resized as a 128x128px image, with aspect ratio preserved. "\
                            "Use this field in form views or some kanban views."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
                           string="Image (auto-resized to 64x64):", type="binary", multi="_get_image",
                           store={
                                  'employee.details': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
                                  },
                           help="Small-sized image of the category. It is automatically "\
                           "resized as a 64x64px image, with aspect ratio preserved. "\
                           "Use this field anywhere a small image is required."),
    }
employee_details()

class project_type(osv.osv):
    _name= 'project.type'
    _columns= {
        'name':fields.char('Project Name'),
        'ref_no':fields.char('project Reference No')
    }
project_type()


class client_master(osv.osv):
    _name= 'client.master'
    _columns= {
        'name':fields.char('Name'),
        'ph_no':fields.integer('Phone Number')
    }
client_master()

class type_of_projects(osv.osv):
    _name= 'type.of.projects'
    _columns= {
        'name':fields.char('Project Type'),
        'reference_no':fields.char('project Reference No')
    }
type_of_projects()