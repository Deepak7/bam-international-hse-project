import math
import openerp.addons.product.product
import logging
import time
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from openerp.tools import  DEFAULT_SERVER_DATETIME_FORMAT

class project_risk_assessment(osv.osv):
    _name = 'project.risk.assessment'
    _description = 'Project Risk Assessment'
    _columns = {
              'name': fields.char('Task Title'),
              'project': fields.many2one('project.type','Project'),
              'entered_by': fields.many2one('res.users','Entered By'),
              'description': fields.html('Description'),
              'dead_line': fields.date('Dead Line'),
              'priority': fields.selection([('0','Normal'), ('1','High')], 'Priority', select=True),
              'status': fields.selection([('new', 'New')],string='Status', readonly=True),
              'active': fields.boolean('Active'),


    }

#-------------------------------------------------------------------------------------------