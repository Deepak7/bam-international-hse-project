import math
import openerp.addons.product.product
import logging
import time
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from openerp.tools import  DEFAULT_SERVER_DATETIME_FORMAT

class incident_general_details(osv.osv):
    _name = 'incident.general.details'
    _description = 'Incident General details'
    _columns = {
              # General columns-----------------
              'project':fields.many2one('project.type','Project ',required=True),
              'client':fields.many2one('client.master','Client'),
              'client_rep':fields.char('Client Rep'),
              'type_of_project':fields.many2one('type.of.projects','Type Of Projects'),
              'time_incident':fields.float('Time'),
              'date_incident':fields.date('Date'),
              'weather_condition':fields.char('Weather/Sea Conditions'),
              'exact_location':fields.char('Exact Location of Incident'),
              'investigation_team':fields.char('Investigation Team'),
              'bam_int':fields.boolean('BAM International'),
              'sub_contractor':fields.boolean('Sub Contractor'),
              'supplier':fields.boolean('Supplier'),
              'client_rep_chk':fields.boolean('Client/Client Representative'),
              'other_cmp':fields.boolean('Other'),
              'other_cmp_type':fields.char('Other Company Type'),
              'person_reported':fields.char('Person reported the incident '),
              'emergency_resp':fields.boolean('Emergency Response'),
              'emergency_resp_note':fields.text('Emergency Response Details'),
              'corrective_action':fields.text('Corrective Action Required'),
              'company_works':fields.char('Company he/she works'),
            #-------------------------------------------------------------

            #______________Incident Details______________________
              'name_ip':fields.char('Name of IP'),
              'induction':fields.char('Induction/Employment No'),
              'start_date_pro':fields.date('Start date on project'),
              'cat1':fields.boolean('Cat1'),
              'cat2':fields.boolean('Cat2'),
              'cat3':fields.boolean('Cat3'),
              'cat4':fields.boolean('Cat4'),
              'no_lost_day':fields.char('Number of lost days'),
              'head':fields.boolean('Head'),
              'back':fields.boolean('Back/Torso'),
              'eye':fields.boolean('Eye'),
              'hand':fields.boolean('Hand/Wrist'),
              'ankle':fields.boolean('Ankle/Foot'),
              'leg':fields.boolean('Leg'),
              'arm':fields.boolean('Arm'),
              'other':fields.boolean('Other'),
              'other_body':fields.char('Other Body part'),
              'abrasion':fields.boolean('Abrasion/Bruising'),
              'burn':fields.boolean('Burn/scald'),
              'electrocution':fields.boolean('Electrocution'),
              'fracture':fields.boolean('Fracture/Break'),
              'laceration':fields.boolean('Laceration/Cut'),
              'internal_injury':fields.boolean('Internal Injury'),
              'amputation':fields.boolean('Amputation'),
              'strain':fields.boolean('Strain/Sprain'),
              'other_injury':fields.boolean('Other'),
              'other_injury_type':fields.char('Other Injury Type'),
              #----------------------------------------------------
              'cat5':fields.boolean('Cat5'),
              'cat6':fields.boolean('Cat6'),
              'vehicle_collision':fields.boolean('Plant/Vehicle Collision'),
              'damage_pro':fields.boolean('Damage to Property'),
              'collapse_structure':fields.boolean('Collapse of Structure'),
              'full_to_water':fields.boolean('Full into Water'),
              'fail_lift':fields.boolean('Failure of Lift Equipment'),
              'electric_fault':fields.boolean('Electric Fault'),
              'fire':fields.boolean('Fire'),
              'collapse_excov':fields.boolean('Collapse Of Excavation'),
              'temp_work':fields.boolean('Temporary '),
              'falling_obj':fields.boolean('Falling Object Outside Exc Zone'),
              'conf_space':fields.boolean('Confined Space'),
              'cont_service':fields.boolean('Contact with service'),
              'other_sub_class':fields.boolean('Other'),
              'other_subclass_type':fields.char('Other Sub Classification'),
            #------------------------------------------------------------------



    }

incident_general_details()