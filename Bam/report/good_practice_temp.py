import time
from openerp.osv import osv
from openerp.report import report_sxw
from datetime import datetime, timedelta
from datetime import date
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT

class good_practice_temp(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(good_practice_temp, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
                    'time': time,

            })

class report_good_practice_temp(osv.AbstractModel):
    _name = 'report.bam.report_good_practice_temp'
    _inherit = 'report.abstract_report'
    _template = 'Bam.report_good_practice_temp'
    _wrapped_report_class = good_practice_temp