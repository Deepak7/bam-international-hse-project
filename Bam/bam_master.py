import math
import openerp.addons.product.product
import logging
import time
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from openerp.tools import  DEFAULT_SERVER_DATETIME_FORMAT

class company_type(osv.osv):
    _name = 'company.type'
    _description = 'Company Type'
    _columns = {
              'name': fields.char('Company Type'),
              'description': fields.char('Description'),
    }


class injury_type(osv.osv):
    _name = 'injury.type'
    _description = 'Injury Type'
    _columns = {
              'name': fields.char('Injury Type'),
              'description': fields.char('Description'),
    }

class body_part(osv.osv):
    _name = 'body.part'
    _description = 'Body Part'
    _columns = {
              'name': fields.char('Body Part'),
              'description': fields.char('Description'),
    }


