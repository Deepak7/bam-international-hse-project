import math
import openerp.addons.product.product
import logging
import time
from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime

import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
from openerp.tools import  DEFAULT_SERVER_DATETIME_FORMAT

class safety_violation(osv.osv):
    _name = 'safety.violation'
    _description = 'Safety Violation'
    _columns = {
              'project':fields.many2one('project.type','Project Name',required=True),
              'location':fields.char('Location'),
              'time':fields.char('Time'),
              'date':fields.date('Date'),
              'trade':fields.char('Trade'),
              'hse_violation_no':fields.char('HSE Violation Number'),
              'description_non_conformance':fields.text('Description of Non Confirmation'),
              'immediate_action':fields.text('Immediate Action Required'),
              'corrective_action':fields.text('Corrective Action Required'),

    }

safety_violation()